import React, { Component } from "react";
import _ from "lodash";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";

class Relevador extends Component {
  state = {
    rol: sessionStorage.getItem("rol"),
    msg: true,
    message: sessionStorage.getItem("msg"),
    usuario: sessionStorage.getItem("usuario"),
    equipo: sessionStorage.getItem("equipo"),
    visitas: [],
    inicio: 0,
    final: 1,
  };

  componentDidMount() {
    axios
      .get("http://localhost:8080/visitas")
      .then((res) => {
        this.setState({
          original: _.cloneDeep(res.data),
          visitas: res.data.slice(this.state.inicio, this.state.final),
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <>
        <div className="row">
          {this.state.msg === true && (
            <span className="alert alert-success text-center">
              <i className="bi bi-check-circle-fill"> </i>
              Conectado {this.state.usuario} del equipo {this.state.equipo}
            </span>
          )}
        </div>
      </>
    );
  }
}

export default Relevador;
