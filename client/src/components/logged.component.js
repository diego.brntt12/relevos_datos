import React, { Component } from "react";
import axios from "axios";

import Monitor from "./monitor.component";
import "bootstrap/dist/css/bootstrap.min.css";
import Relevador from "./relevador.component";

class Logged extends Component {
  state = {
    rol: sessionStorage.getItem("rol"),
  };


  render() {
    return (
      <>
        {this.state.rol === "Monitor" && <Monitor />}
        {this.state.rol === "Relevador" && <Relevador />}
      </>
    );
  }
}

export default Logged;