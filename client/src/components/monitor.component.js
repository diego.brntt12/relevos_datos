import React, { Component } from "react";
import _ from "lodash";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";

class Monitor extends Component {
  state = {
    rol: sessionStorage.getItem("rol"),
    msg: true,
    message: sessionStorage.getItem("msg"),
    usuario: sessionStorage.getItem("usuario"),
    equipo: sessionStorage.getItem("equipo"),
    visitas: [],
    inicio: 0,
    final: 1,
  };

  componentDidMount() {
    axios
      .get("http://localhost:8080/visitas")
      .then((res) => {
        this.setState({
          original: _.cloneDeep(res.data),
          visitas: res.data.slice(this.state.inicio, this.state.final),
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <>
        <h1 className="text-center">VISITAS</h1>
        <div className="row">
          {this.state.msg === true && (
            <span className="alert alert-success text-center">
              <i className="bi bi-check-circle-fill"> </i>
              Conectado {this.state.usuario} del equipo {this.state.equipo}
            </span>
          )}
        </div>

        <div className="table-responsive">
          <table className="table table-hover w-100">
            <thead>
              <tr className="text-center align-middle">
                <th scope="col">Usuario</th>
                <th scope="col">Acopio (dirección)</th>
                <th scope="col">Fecha</th>
                <th scope="col">Estado</th>
              </tr>
            </thead>
            <tbody>
              {this.state.visitas.map((visita) => {
                return (
                  <>
                    <tr className="text-center align-middle">
                      <td className="w-50">{visita.usuario.nombre}</td>
                      <td className="w-50">{visita.acopio.direccion} </td>
                      <td className="w-20">{visita.fecha} </td>
                      <td className="text-center w-20 justify-content-center">
                        {visita.cerrada === true ? "CERRADA" : "ABIERTA"}
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
        </div>
        <div className="table-responsive">
          <table className="table table-hover w-100">
            <thead>
              <tr className="text-center align-middle w-100">
                <th scope="col">Categoria</th>
                <th scope="col">SubCategoria</th>
                <th scope="col">Equipo</th>
                <th scope="col">Dato</th>
                <th scope="col">Observacion</th>
                <th scope="col">Foto</th>
              </tr>
            </thead>
            <tbody>
              {this.state.visitas.map((visita) => {
                return visita.acopio.categorias.map((categoria) => {
                  return categoria.subcategorias.map((subcategoria) => {
                    return (
                      <tr className="text-center align-middle w-100">
                        <td className="col">{categoria.nombre}</td>
                        <td className="col">{subcategoria.nombre}</td>
                        <td className="col">{categoria.equipo.nombre}</td>
                        <td className="col">{subcategoria.datoRelevo}</td>
                        <td className="col">{subcategoria.observacion}</td>
                        <td className="col">
                          {subcategoria.foto !== null ? (
                            <img
                              src={URL.createObjectURL(new Blob( subcategoria.foto.data, { type: "image/png" } ))}
                              alt=""
                            />
                          ) : (
                            "No se precisa foto de esta subcategoria"
                          )}
                        </td>
                      </tr>
                    );
                  });
                });
              })}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default Monitor;
