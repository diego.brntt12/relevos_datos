import axios from "axios";
import React, { Component, Redirect } from "react";
import { Link } from "react-router-dom";

class NavHome extends Component {
  state = {
    user: "",
  };

  handleSignup = async () => {
    this.setState({
      signupView: true,
    });
  };

  handleLogin = async () => {
    this.setState({
      signupView: false,
    });
  };

  handleLogout = async () => {
    sessionStorage.clear();
    window.location.reload();
  };


  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand mx-2">
          Relevos de datos
        </Link>
        <div className="collpase navbar-collapse d-flex justify-content-end">
          <ul className="navbar-nav mx-2">
            {sessionStorage.getItem("logged") !== null && (
              <li className="navbar-item">
                <Link
                  to="/"
                  className="btn btn-outline-secondary"
                  onClick={this.handleLogout}
                >
                  Logout
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavHome;