import React, { Component } from "react";
import axios from "axios";

import "bootstrap/dist/css/bootstrap.min.css";
import Logged from "./logged.component";

class Login extends Component {
  state = {
    form: {
      nombre: "",
      pass: "",
    },
    error: false,
    errorMsg: "",
    logged: false,
  };

  handleChange = async (event) => {
    await this.setState({
      form: {
        ...this.state.form,
        [event.target.name]: event.target.value,
      },
    });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
  };

  handleBotton = async () => {
    await axios
      .post(`http://localhost:8080/usuarios/login`, this.state.form)
      .then((res) => {
        if (res.data.status === "Success") {
          sessionStorage.setItem("rol", res.data.data.rol.nombre);
          sessionStorage.setItem("usuario", res.data.data.nombre);
          sessionStorage.setItem("equipo", res.data.data.equipo.nombre);
          sessionStorage.setItem("msg", res.data.message);
          sessionStorage.setItem("logged", true);

          this.setState({
            logged: true,
          });

          console.log(res.data);

          window.location.reload(true);
        } else if (res.data.status === "Failed") {
          this.setState({
            error: true,
            errorMsg: res.data.message,
          });
          console.log(res.data);
        }
      })
      .catch((error) => {
        this.setState({
          error: true,
          errorMsg: "Connection failed",
        });
      });
  };

  render() {
    if (sessionStorage.getItem("logged") === null) {
      return (
        <div className="container d-grid">
          <h1>Login</h1>

          <form onSubmit={this.handleSubmit}>
            <div className="row form-floating mb-3">
              <input
                type="nombre"
                name="nombre"
                className="form-control"
                placeholder="Nombre de usuario"
                onChange={this.handleChange}
              />

              <label htmlFor="nombre">
                <i className="bi bi-envelope"></i> Nombre de usuario
              </label>
            </div>

            <div className="row form-floating">
              <input
                type="password"
                name="pass"
                className="form-control"
                placeholder="Contraseña"
                onChange={this.handleChange}
              />
              <label htmlFor="pass">
                <i className="bi bi-lock"></i> Password
              </label>
            </div>
            <br />
            <div className="row">
              {this.state.error === true && (
                <span className="alert alert-danger text-center">
                  <i className="bi bi-check-circle-fill"> </i>
                  {this.state.errorMsg}
                </span>
              )}
            </div>
            <br />
            <input
              type="submit"
              value="Login"
              className="btn btn-primary"
              onClick={this.handleBotton}
            />
          </form>
        </div>
      );
    } else {
      return <Logged />;
    }
  }
}

export default Login;
