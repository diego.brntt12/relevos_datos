import React, { Component } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import axios from "axios"
import "bootstrap/dist/css/bootstrap.min.css";

import NavHome from "./components/nav.component"; 
import Login from "./components/login.component"; 

class App extends Component {
  render() {
    return (
      <div className="w-100">
        <Router>
          <NavHome />
          <br />
          <Routes>
            <Route path="/" element={<Login />} />
          </Routes>
        </Router>
      </div>
    );
  }
}

export default App;