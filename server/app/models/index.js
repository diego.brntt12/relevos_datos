const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.usuarios = require("./usuario.model")(sequelize, Sequelize);
db.equipos = require("./equipo.model")(sequelize, Sequelize);
db.roles = require("./rol.model")(sequelize, Sequelize);
db.acopios = require("./acopio.model")(sequelize, Sequelize);
db.categorias = require("./categoria.model")(sequelize, Sequelize);
db.subCategorias = require("./subCategoria.model")(sequelize, Sequelize);
db.visitas = require("./visita.model")(sequelize, Sequelize);

db.usuarios.belongsTo(db.equipos, {foreignKey: 'equipoId', as:'equipo'})
db.usuarios.belongsTo(db.roles, {foreignKey: 'rolId', as:'rol'})

db.visitas.belongsTo(db.usuarios, {foreignKey: 'usuarioId', as:'usuario'})
db.visitas.belongsTo(db.acopios, {foreignKey: 'acopioId', as:'acopio'})

//db.acopios.hasMany(db.categorias, {foreignKey: 'acopioId',as:'categorias'})
db.acopios.hasMany(db.categorias, {foreignKey: 'acopioId',as:'categorias'})
db.categorias.belongsTo(db.equipos, {foreignKey: 'equipoId', as:'equipo'})

db.categorias.hasMany(db.subCategorias, {foreignKey: 'categoriaId',as:'subcategorias'})
db.subCategorias.belongsTo(db.categorias, {foreignKey: 'categoriaId', as: 'categoria'})




module.exports = db;
