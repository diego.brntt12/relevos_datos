module.exports = (sequelize, Sequelize) => {
    const Visita = sequelize.define("visita", {
      usuarioId: {
        type: Sequelize.INTEGER
      },
      acopioId: {
        type: Sequelize.INTEGER
      },
      cerrada: {
        type: Sequelize.BOOLEAN
      },
      fecha: {
        type: Sequelize.DATEONLY
      },
    });

    return Visita;
  };