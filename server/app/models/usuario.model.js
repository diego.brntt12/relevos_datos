module.exports = (sequelize, Sequelize) => {
    const Usuario = sequelize.define("usuario", {
      nombre: {
        type: Sequelize.STRING
      },
      pass: {
        type: Sequelize.STRING
      },
      rolId: {
        type: Sequelize.INTEGER
      },
      equipoId: {
        type: Sequelize.INTEGER
      }
    });

    

    return Usuario;
  };