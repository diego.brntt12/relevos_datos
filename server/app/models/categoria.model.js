module.exports = (sequelize, Sequelize) => {
  const Categoria = sequelize.define("categoria", {
    nombre: {
      type: Sequelize.STRING,
    },
    equipoId: {
      type: Sequelize.INTEGER,
    },
  });

  return Categoria;
};
