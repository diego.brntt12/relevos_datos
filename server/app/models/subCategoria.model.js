const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const SubCategoria = sequelize.define("subCategoria", {
      nombre: {
        type: Sequelize.STRING
      },
      datoRelevo: {
        type: Sequelize.STRING
      },
      observacion: {
        type: Sequelize.STRING
      },
      foto: {
        type: Sequelize.BLOB
      },
      categoriaId: {
        type: Sequelize.INTEGER
      },
    });
  
    return SubCategoria;
  };