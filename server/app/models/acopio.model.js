module.exports = (sequelize, Sequelize) => {
    const Acopio = sequelize.define("acopio", {
      direccion: {
        type: Sequelize.STRING
      },
    });
  
    return Acopio;
  };