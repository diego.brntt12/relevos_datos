module.exports = app => {
    const acopios = require("../controllers/acopio.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", acopios.create);

    router.get("/", acopios.findAll);
  
    app.use('/acopios', router);
  };
  