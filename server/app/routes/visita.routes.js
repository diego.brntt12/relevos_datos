module.exports = app => {
    const visitas = require("../controllers/visita.controller");
  
    var router = require("express").Router();
  
    router.post("/", visitas.create);
    router.get("/", visitas.findAll);
    router.put("/:id", visitas.update);
  
    app.use('/visitas', router);
  };
  