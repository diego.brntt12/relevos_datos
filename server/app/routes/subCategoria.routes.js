module.exports = app => {
    const subCategoria = require("../controllers/subCategoria.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", subCategoria.create);
    router.get("/", subCategoria.findAll);
    router.put("/:id", subCategoria.update);
  
    app.use('/subcategorias', router);
  };
  