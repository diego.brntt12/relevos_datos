module.exports = app => {
    const categorias = require("../controllers/categoria.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", categorias.create);
    router.get("/", categorias.findAll);
    router.put("/", categorias.update);
  
    app.use('/categorias', router);
  };
  