module.exports = app => {
    const equipos = require("../controllers/equipo.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", equipos.create);
    router.get("/", equipos.findAll);
  
    app.use('/equipos', router);
  };
  