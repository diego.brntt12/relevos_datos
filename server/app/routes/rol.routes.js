module.exports = app => {
    const roles = require("../controllers/rol.controller");
  
    var router = require("express").Router();
  
    // Create a new Tutorial
    router.post("/", roles.create);

    router.get("/", roles.findAll);
  
    app.use('/roles', router);
  };
  