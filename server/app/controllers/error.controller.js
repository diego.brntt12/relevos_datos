exports.verificar = (body, req, res) => {
// Comprobación de campos en blanco
  for ([campo, valor] of body) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }
}