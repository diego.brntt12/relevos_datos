const db = require("../models");
const SubCategoria = db.subCategorias;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {

  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }

  let subCategoria = {
    nombre: req.body.nombre,
    datoRelevo: req.body.datoRelevo,
    observacion: req.body.observacion,
    categoriaId: req.body.categoriaId,
  };

  await SubCategoria.create(subCategoria)
    .then((data) => {
      return res.send(data);
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message || "Ocurrio algun error a la hora de crear la catregoria.",
      });
    });
};

exports.findAll = async (req, res) => {
  await SubCategoria.findAll({
    include: 'categoria'
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

exports.update = async (req, res) => {
  await SubCategoria.update(req.body, {
    where: { id: req.params.id },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};