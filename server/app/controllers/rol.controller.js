const db = require("../models");
const Rol = db.roles;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {

  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }

  let rol = {
    nombre: req.body.nombre,
  };

  await Rol.findOne({where: {nombre: rol.nombre}}).then(data =>{
    return res.status(400).send({
      message: `La categoria ${data.nombre} ya existe.`,
    });
  }).catch(err => {
    Rol.create(rol)
    .then((data) => {
      return res.send(data);
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message || "Ocurrio algun error a la hora de crear la catregoria.",
      });
    });
  })

};

exports.findAll = async (req, res) => {
  await Rol.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};