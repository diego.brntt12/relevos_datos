const db = require("../models");
const Categoria = db.categorias;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {

  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacio`,
      });
    }
  }

  let categoria = {
    nombre: req.body.nombre,
    equipoId: req.body.equipoId,
    acopioId: req.body.acopioId,
  };

  await Categoria.create(categoria)
    .then((data) => {
      return res.send(data);
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message || "Ocurrio algun error a la hora de crear el punto a medir.",
      });
    });
};

exports.findAll = async (req, res) => {
  await Categoria.findAll({
    include: ['equipo', 'subcategorias']
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

exports.update = async (req, res) => {
  
  await Categoria.update(req.body, {
    where: { id: req.body.id },
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};