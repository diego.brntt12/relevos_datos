const db = require("../models");
const Acopio = db.acopios;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {

  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacio`,
      });
    }
  }

  let acopio = {
    direccion: req.body.direccion,
  };

  await Acopio.create(acopio)
    .then((data) => {
      return res.send(data);
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message || "Ocurrio algun error a la hora de crear la categoria.",
      });
    });
};

exports.findAll = async (req, res) => {
  await Acopio.findAll({
    include: {
                model: db.categorias,
                include: ['subcategorias', 'equipo'],
                as: 'categorias',
              }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};