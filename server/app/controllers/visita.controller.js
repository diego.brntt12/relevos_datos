const db = require("../models");
const Visita = db.visitas;
const Op = db.Sequelize.Op;

exports.create = async (req, res) => {
  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacio`,
      });
    }
  }

  await Visita.findOne({
    where: {
      usuarioId: req.body.usuarioId,
      acopioId: req.body.acopioId,
      fecha: Date.now(),
    },
  })
    .then((data) => {
      if (!data) {
        Visita.create(visita)
          .then((data) => {
            return res.send(data);
          })
          .catch((err) => {
            return res.send({
              message:
                err.message ||
                "Ocurrio algun error a la hora de crear el punto a medir.",
            });
          });
      } else {
        return res.send({
          message:
            "Ya hay una visita registrada en la fecha actual dentro del acopio",
        });
      }
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message ||
          "Ocurrio algun error a la hora de crear el punto a medir.",
      });
    });
};

exports.findAll = async (req, res) => {
  await Visita.findAll({
    include: [
      {
        model: db.acopios,
        as: "acopio",
        include: {
          model: db.categorias,
          include: ["subcategorias", "equipo"],
          as: "categorias",
        },
      },
      {
        model: db.usuarios,
        as: "usuario",
        include: ["equipo"],
      },
    ],
  })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials.",
      });
    });
};

exports.update = async (req, res) => {
  await Visita.findOne({
    where: { id: req.params.id },
    include: [
      {
        model: db.acopios,
        as: "acopio",
        include: {
          model: db.categorias,
          include: ["subcategorias", "equipo"],
          as: "categorias",
        },
      },
      {
        model: db.usuarios,
        as: "usuario",
        include: {
          model: db.equipos,
          include: ["equipo"],
          as: "equipo",
        },
      },
    ],
  }).then(data => {
    if (data.cerrada == true) {
      res.send({
        status: "failed",
        message: "Esta visita ya se encuentra cerrada",
      })
    } else {
      Visita.update(req.body, {
        where: { id: req.params.id },
      })
        .then((data) => {
          res.send(data);
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving tutorials.",
          });
        });
    }
  });
};
