const db = require("../models");
const Equipo = db.equipos;
const Op = db.Sequelize.Op;


exports.create = async (req, res) => {
  
  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }

  let equipo = {
    nombre: req.body.nombre,
  };

  await Equipo.findOne({where: {nombre: equipo.nombre}}).then(data =>{
    return res.status(400).send({
      message: `El equipo ${data.nombre} ya existe.`,
    });
  }).catch(err => {
    Equipo.create(equipo)
    .then((data) => {
      return res.send(data);
    })
    .catch((err) => {
      return res.status(500).send({
        message:
          err.message || "Ocurrio algun error a la hora de crear el equipo.",
      });
    });
  })
};

exports.findAll = async (req, res) => {
  await Equipo.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Hubo algún pproblema al consultar los equipos."
      });
    });
};