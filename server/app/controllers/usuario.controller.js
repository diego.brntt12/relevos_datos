const db = require("../models");
const bcrypt = require("bcrypt");
const Usuario = db.usuarios;
const Categoria = db.categorias;
const Op = db.Sequelize.Op;

// Creación de un nuevo usuario
exports.create = async (req, res) => {
  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.status(400).send({
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }

  await Usuario.findOne({ where: { nombre: req.body.nombre } })
    .then((data) => {
      return res.status(400).send({
        message: `El usuario ${data.nombre} ya existe.`,
      });
    })
    .catch((err) => {
      // Crear un  usuario encriptando la contraseña
      bcrypt.hash(req.body.pass, 10, (error, hashPass) => {
        if (error) throw error;

        let usuario = {
          nombre: req.body.nombre,
          pass: hashPass,
          rolId: req.body.rolId,
          equipoId: req.body.equipoId,
        };

        Usuario.create(usuario)
          .then((data) => {
            return res.json(data);
          })
          .catch((err) => {
            return res.status(500).json({
              message:
                err.message ||
                "Ocurrio algun error a la hora de crear el usuario.",
            });
          });
      });
    });
};

exports.login = async (req, res) => {
  // Comprobación de campos en blanco
  for ([campo, valor] of Object.entries(req.body)) {
    if (!valor) {
      return res.send({
        status: "Failed",
        message: `El campo ${campo} no puede estar vacia`,
      });
    }
  }

  await Usuario.findOne({
    where: { nombre: req.body.nombre },
    include: ["rol","equipo"],
  })
    .then((data) => {
      // Crear un  usuario encriptando la contraseña
      bcrypt.compare(req.body.pass, data.pass, (error, isValid) => {
        if (isValid) {
          return res.send({
            status: "Success",
            data: data,
            message: "usuario logueado correctamente.",
          });
        } else {
          return res.send({
            status: "Failed",
            message: "Contraseña incorrecta.",
          });
        }
      });
    })
    .catch((err) => {
      return res.send({
        status: "Failed",
        message: "Usuario incorrecto.",
      });
    });
};

exports.findAll = async (req, res) => {
  await Usuario.findAll({ 
    include: ["rol","equipo"],
   })
    .then((data) => {
      res.json(data);
    })
    .catch((err) => {
      res.status(500).json({
        message:
          err.message || "Hubo algún pproblema al consultar los usuarios.",
      });
    });
};
