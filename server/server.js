require('dotenv').config();
const express = require("express");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync({force: false})
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

require("./app/routes/usuario.routes")(app);
require("./app/routes/equipo.routes")(app);
require("./app/routes/rol.routes")(app);
require("./app/routes/categoria.routes")(app);
require("./app/routes/subCategoria.routes")(app);
require("./app/routes/acopio.routes")(app);
require("./app/routes/visita.routes")(app);

const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
